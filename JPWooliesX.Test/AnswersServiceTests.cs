using System;
using System.Collections.Generic;
using System.Linq;
using JPWooliesX.HttpClients;
using JPWooliesX.Models;
using JPWooliesX.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace JPWooliesX.Test
{
    [TestClass]
    public class AnswersServiceTests
    {
        [TestMethod]
        public void GetUser_ReturnsDefaultUser()
        {
            var mockConfig = new Mock<IConfigService>();
            mockConfig.Setup(x => x.Get("Name")).Returns("test");
            mockConfig.Setup(x => x.Get("Token")).Returns("1234");
            var service = new AnswersService(null, mockConfig.Object);
            var result = service.GetTestUser();
            Assert.AreEqual("test", result.Name);
            Assert.AreEqual("1234", result.Token);
        }

        [DataTestMethod]
        [DataRow(SortOption.Low, "cheapest", "expensive")]
        [DataRow(SortOption.High, "expensive", "cheapest")]
        [DataRow(SortOption.Ascending, "aadvark", "zebra")]
        [DataRow(SortOption.Descending, "zebra", "aadvark")]
        [DataRow(SortOption.Recommended, "popular", "zebra")]
        public void GetOrderedProducts_OrdersBySortOption(SortOption option, string expectedFirstProductName, string expectedLastProductName)
        {
            var mockClient = new Mock<IWooliesXClient>();
            mockClient.Setup(x => x.GetProductsAsync()).ReturnsAsync(this.GetSampleProducts());
            mockClient.Setup(x => x.GetShopperHistoryAsync()).ReturnsAsync(this.GetSampleShopperHistory());
            var service = new AnswersService(mockClient.Object, null);
            var result = service.GetSortedProductsAsync(option).GetAwaiter().GetResult();
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedFirstProductName, result.FirstOrDefault().Name);
            Assert.AreEqual(expectedLastProductName, result.LastOrDefault().Name);
            Assert.AreEqual(this.GetSampleProducts().Count, result.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "thrown")]
        public void GetOrderedProducts_ThrowsExceptionsFromClient()
        {
            var mockClient = new Mock<IWooliesXClient>();
            mockClient.Setup(x => x.GetProductsAsync()).ThrowsAsync(new Exception("thrown"));
            var service = new AnswersService(mockClient.Object, null);
            var result = service.GetSortedProductsAsync(SortOption.Low).GetAwaiter().GetResult();
        }

        [TestMethod]
        public void CalculateTrolleyTotal_CallsClientToCalculate()
        {
            var trolleyUsed = new Trolley { Specials = new List<Special>() { new Special { Total = 666 } } };
            var trolleyPassedOn = new Trolley();
            var expectedTotal = 10.1;
            var mockClient = new Mock<IWooliesXClient>();
            mockClient.Setup(x => x.CalculateTrollyTotalAsync(It.IsAny<Trolley>())).Callback<Trolley>(x => trolleyPassedOn = x).ReturnsAsync(expectedTotal);
            var service = new AnswersService(mockClient.Object, null);
            var result = service.CalculateTrollyTotalAsync(trolleyUsed).GetAwaiter().GetResult();
            Assert.AreEqual(trolleyUsed, trolleyPassedOn);
            Assert.AreEqual(expectedTotal, result);
        }

        private List<Product> GetSampleProducts()
        {
            var json = @"[
                    {
                        ""name"":""prod1"",
                        ""price"": 399.99,
                        ""quantity"": 2
                    },
                    {
                        ""name"":""cheapest"",
                        ""price"": 299.99,
                        ""quantity"": 3
                    },
                    {
                        ""name"":""popular"",
                        ""price"": 499.99,
                        ""quantity"": 4
                    },
                    {
                        ""name"":""expensive"",
                        ""price"": 599.99,
                        ""quantity"": 2.5
                    },
                    {
                        ""name"":""aadvark"",
                        ""price"": 450.99,
                        ""quantity"": 4
                    },    
                    {
                        ""name"":""zebra"",
                        ""price"": 350,
                        ""quantity"": 3
                    }
                ]";
            return JsonConvert.DeserializeObject<List<Product>>(json);
        }

        private List<ShopperHistory> GetSampleShopperHistory()
        {
            var json = @"[
                            {
                                ""customerId"": 1,
                                ""products"": [
                                    {
                                        ""name"": ""prod1"",
                                        ""price"": 399.99,
                                        ""quantity"": 2
                                    }
                                ]
                            },
                            {
                                ""customerId"": 3,
                                ""products"": [
                                    {
                                        ""name"": ""expensive"",
                                        ""price"": 599.99,
                                        ""quantity"": 2.5
                                    }
                                ]
                            },
                            {
                                ""customerId"": 56,
                                ""products"": [
                                    {
                                        ""name"": ""popular"",
                                        ""price"": 499.99,
                                        ""quantity"": 4
                                    },
                                    {
                                        ""name"": ""popular"",
                                        ""price"": 499.99,
                                        ""quantity"": 4
                                    },
                                    {
                                        ""name"": ""popular"",
                                        ""price"": 499.99,
                                        ""quantity"": 4
                                    }
                                ]
                            },
                            {
                                ""customerId"": 100,
                                ""products"": [
                                    {
                                        ""name"": ""expensive"",
                                        ""price"": 599.99,
                                        ""quantity"": 2.5
                                    },
                                    {
                                        ""name"": ""popular"",
                                        ""price"": 499.99,
                                        ""quantity"": 4
                                    }
                                ]
                            }
                            ]";
            return JsonConvert.DeserializeObject<List<ShopperHistory>>(json);
        }
    }
}
