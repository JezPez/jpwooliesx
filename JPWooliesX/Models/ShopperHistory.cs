using System;
using System.Collections.Generic;

namespace JPWooliesX
{
    public class ShopperHistory
    {
        public int? CustomerId { get; set; }
        public List<Product> Products { get; set; }
    }
}