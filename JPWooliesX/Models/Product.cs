using System;

namespace JPWooliesX
{
    public class Product
    {
        public string Name { get; set; }
        public double? Price { get; set; }
        public double? Quantity { get; set; }
    }
}