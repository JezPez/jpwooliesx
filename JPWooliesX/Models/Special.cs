using System.Collections.Generic;

namespace JPWooliesX.Models
{
    public class Special
    {
        public List<ProductQuantity> Quantities { get; set; }
        public double Total { get; set; }
    }
}