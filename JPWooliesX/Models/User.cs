using System;

namespace JPWooliesX
{
    public class User
    {
        /// <summary>
        /// Default name property is "test"
        /// </summary>
        public string Name { get; set; } = "test";

        /// <summary>
        /// Default token property is a string "1234-455662-22233333-3333"
        /// </summary>
        public string Token { get; set; } = "1234-455662-22233333-3333";
    }
}