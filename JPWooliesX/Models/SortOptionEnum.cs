using System;

namespace JPWooliesX
{
    public enum SortOption  
    {
        Low,
        High,
        Ascending,
        Descending,
        Recommended
    }
}