﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JPWooliesX.Models;
using JPWooliesX.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace JPWooliesX.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnswersController : ControllerBase
    {
        private readonly IAnswersService answers;

        public AnswersController(IAnswersService answers)
        {
            this.answers = answers;
        }

        [HttpGet("user")]
        public User Get()
        {
            return answers.GetTestUser();
        }

        [HttpGet("sort")]
        public async Task<List<Product>> GetSortedProducts([FromQuery] string sortOption)
        {
            var sortOptionIsValid = Enum.TryParse(typeof(SortOption), sortOption, true, out var sortOptionEnum);
            return await answers.GetSortedProductsAsync(sortOptionIsValid ? (SortOption)sortOptionEnum : SortOption.Low);
        }

        // In the test it's documented as /TrolleyCalculator but that is the internal API endpoint
        [HttpPost("trolleyTotal")] 
        public async Task<double> GetTrollyTotal([FromBody] Trolley trolley)
        {
            return await answers.CalculateTrollyTotalAsync(trolley);
        }
    }
}
