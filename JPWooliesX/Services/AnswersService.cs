using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JPWooliesX.HttpClients;
using JPWooliesX.Models;
using Microsoft.Extensions.Configuration;

namespace JPWooliesX.Services
{
    public class AnswersService : IAnswersService
    {
        private readonly IWooliesXClient client;
        private readonly string name;
        private readonly string token;

        public AnswersService(IWooliesXClient client, IConfigService config)
        {
            this.client = client;
            if (config != null)
            {
                this.name = config.Get("Name");
                this.token = config.Get("Token");
            }
        }

        public User GetTestUser()
        {
            return string.IsNullOrEmpty(this.name) && string.IsNullOrEmpty(this.token) ? new User() : new User
            {
                Name = this.name,
                Token = this.token
            };
        }

        public async Task<List<Product>> GetSortedProductsAsync(SortOption sortOption)
        {
            var products = await this.client.GetProductsAsync();
            switch (sortOption)
            {
                case SortOption.Low:
                    products = products.OrderBy(x => x.Price).ToList();
                    break;
                case SortOption.High:
                    products = products.OrderByDescending(x => x.Price).ToList();
                    break;
                case SortOption.Ascending:
                    products = products.OrderBy(x => x.Name).ToList();
                    break;
                case SortOption.Descending:
                    products = products.OrderByDescending(x => x.Name).ToList();
                    break;
                case SortOption.Recommended:
                    var shopperHistory = await this.client.GetShopperHistoryAsync();
                    var productsMostPopularFirst = shopperHistory.SelectMany(x => x.Products)
                        .GroupBy(x => x.Name)
                        .OrderByDescending(y => y.Count())
                        .Select(x => x.FirstOrDefault()).ToList();
                    productsMostPopularFirst.AddRange(products.Where(x => !productsMostPopularFirst.Any(y => x.Name == y.Name)));
                    products = productsMostPopularFirst;
                    break;
                default:
                    break;
            }
            return products;
        }

        public async Task<double> CalculateTrollyTotalAsync(Trolley trolley)
        {
            return await this.client.CalculateTrollyTotalAsync(trolley);
        }
    }

}