namespace JPWooliesX.Services
{
    public interface IConfigService
    {
        string Get(string key);
    }
    
}