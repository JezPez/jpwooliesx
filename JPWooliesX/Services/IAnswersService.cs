using System.Collections.Generic;
using System.Threading.Tasks;
using JPWooliesX.Models;

namespace JPWooliesX.Services
{
    public interface IAnswersService
    {
        User GetTestUser();
        Task<List<Product>> GetSortedProductsAsync(SortOption sortOption);
        Task<double> CalculateTrollyTotalAsync(Trolley trolley);
    }
    
}