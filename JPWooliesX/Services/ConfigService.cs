using Microsoft.Extensions.Configuration;

namespace JPWooliesX.Services
{
    /// <summary>
    /// Abstracted out to make unit testing easier
    /// </summary>
    public class ConfigService : IConfigService
    {
        private readonly IConfiguration config;

        public ConfigService(IConfiguration config)
        {
            this.config = config;
        }

        public string Get(string key)
        {
            return this.config.GetValue<string>(key);
        }
    }
    
}