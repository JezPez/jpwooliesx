using System.Collections.Generic;
using System.Threading.Tasks;
using JPWooliesX.Models;

namespace JPWooliesX.HttpClients
{
    public interface IWooliesXClient
    {
        Task<List<Product>> GetProductsAsync();

        Task<List<ShopperHistory>> GetShopperHistoryAsync();

        Task<double> CalculateTrollyTotalAsync(Trolley trolley);
    }
}