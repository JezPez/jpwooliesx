using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using JPWooliesX.Models;
using JPWooliesX.Services;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace JPWooliesX.HttpClients
{
    public class WooliesXClient : IWooliesXClient
    {
        private readonly HttpClient _client;
        private readonly string _token;


        public WooliesXClient(HttpClient client, IConfigService config)
        {
            client.BaseAddress = new Uri("http://dev-wooliesx-recruitment.azurewebsites.net/api/resource/");
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            this._client = client;
            this._token = config.Get("Token");
        }

        public async Task<List<Product>> GetProductsAsync()
        {
            var productsJson = await _client.GetStringAsync($"products?token={_token}");
            return JsonConvert.DeserializeObject<List<Product>>(productsJson);
        }

        public async Task<List<ShopperHistory>> GetShopperHistoryAsync()
        {
            var shopperHistoryJson = await _client.GetStringAsync($"shopperHistory?token={_token}");
            return JsonConvert.DeserializeObject<List<ShopperHistory>>(shopperHistoryJson);
        }

        public async Task<double> CalculateTrollyTotalAsync(Trolley trolley)
        {
            var result = await _client.PostAsJsonAsync($"trolleyCalculator?token={_token}", trolley);
            var totalString = await result.Content.ReadAsStringAsync();
            return double.Parse(totalString);
        }
    }
}